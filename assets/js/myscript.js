const flashdata =  $('.flash-data').data('flashdata');
const alertdata =  $('.flash-data').data('alertdata');

console.log(flashdata);

if ( flashdata ) {
    Swal.fire({
        title: 'Data Mahassswa',
        text: 'Berhasil ' + flashdata,
        icon: alertdata,
        confirmButtonText: 'Ok',
        timer: 1500
      });
    
}

$('.destroy').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "data mahasiswa akan dihapus",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
         document.location.href = href;
        }
      })

});