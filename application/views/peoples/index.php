<div class="container mt-4">
    <div class="row">
        <div class="col-6">
            <h3>List of Peoples</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <form action="<?= base_url('peoples'); ?>" method="post">
                 <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="search keyword" aria-label="Recipient's username" aria-describedby="button-addon2" name="keyword" autocomplete="true" autofocus="true">
                    <input class="btn btn-outline-secondary" type="submit" id="button-addon2" name="submit"></input>
                </div>
            </form>
            <br>
            <h5>Results : <?= $total_rows; ?></h5>
        </div>
    </div>
    <div class="row">
       <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>  
                <?php if ( empty($peoples) ) : ?>
                    <tr>
                        <td colspan="5">
                            <div class="alert alert-danger" role="alert">
                                Data not found!.
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>              
                <?php foreach ($peoples as $people) : ?>
                    <tr>
                    <td><?= ++$start; ?></td>
                    <td><?= $people['name']; ?></td>
                    <td><?= $people['address']; ?></td>
                    <td><?= $people['email']; ?></td>
                    <td>
                        <a href="#" class="badge bg-primary rounded-pill">Detail</a>
                        <a href="#" class="badge bg-warning rounded-pill mx-1">Edit</a>
                        <a href="#" class="badge bg-danger rounded-pill">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
       </table>
    </div>
    <?= $this->pagination->create_links(); ?>
</div>