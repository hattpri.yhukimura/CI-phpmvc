<div class="container mt-4">
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3>Detail Mahasiswa</h3>
                </div>
                <div class="card-body">
                    <div class="card-subtitle">
                        <h5><?= $mahasiswa['nama']; ?></h5>
                    </div>
                    <div class="card-text">
                        <?= $mahasiswa['nik']; ?>
                    </div>
                    <div class="card-text">
                        <?= $mahasiswa['email']; ?>
                    </div>
                    <div class="card-text">
                        <?= $mahasiswa['jurusan']; ?>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="<?= base_url(); ?>mahasiswa" class="btn btn-sm btn-secondary rounded-pill float-end">back</a>
                </div>
            </div>
        </div>
    </div>
</div>