<div class="container mt-4">

    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3>Edit Daftar Mahasiswa</h3>
                </div>
                <div class="card-body">
                
                    <form action="" method="post">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="name" placeholder="Name" name="nama" value="<?= $mahasiswa['nama'] ?>">
                            <label for="name">Name</label>  
                            <small id="name" class="form-text text-danger"><?= form_error('nama'); ?></small>                          
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nik"  placeholder="Nik" name="nik" value="<?= $mahasiswa['nik'] ?>">
                            <label for="nik">Nik</label>
                            <small id="name" class="form-text text-danger"><?= form_error('nik'); ?></small>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="email"  placeholder="Email" name="email" value="<?= $mahasiswa['email'] ?>">
                            <label for="email">Email</label>
                            <small id="name" class="form-text text-danger"><?= form_error('email'); ?></small>
                        </div>
                        <div class="form-floating mb-3">
                            <select class="form-select" id="jurusan" aria-label="Pilih Jurusan" name="jurusan" value="<?= $mahasiswa['jurusan'] ?>">
                                <option selected>-Pilih-</option>
                                <option value="Teknik Informatika" 
                                <?php if ($mahasiswa['jurusan'] == 'Teknik Informatika') :?>
                                    selected
                                <?php endif; ?>
                                    >Teknik Informatika</option>
                                <option value="Ekonomi"
                                <?php if ($mahasiswa['jurusan'] == 'Ekonomi') :?>
                                    selected
                                <?php endif; ?>
                                    >Ekonomi</option>
                            </select>
                            <label for="jurusan">-Juruusan-</label>
                        </div>
                         <a href="<?= base_url(); ?>mahasiswa" class="btn btn-sm btn-secondary rounded-pill float-end mx-1">Back</a>           
                        <button type="submit" class="btn btn-sm btn-primary rounded-pill float-end">Submit</button>
                    </form>
                </div>
            </div>           
        </div>
    </div>

</div>