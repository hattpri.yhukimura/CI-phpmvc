
<div class="container mt-4"> 
    <div class="row">
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash');?>" data-alertdata="<?= $this->session->flashdata('alert'); ?>"></div>
    </div>
    <div class="row mb-3">
        <div class="col-6">
            <a class="btn btn-sm btn-primary rounded-pill" href="<?= base_url(); ?>mahasiswa/tambah">Tambah Data Mahasiswa</a>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-6">
            <form action="" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="cari mahasiswa. ..." aria-label="Recipient's username" aria-describedby="cari" name="keyword" id="keyword" autocomplate="off">
                    <button class="btn btn-outline-primary" type="submit" id="cari">Cari</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <h3>Daftar Mahasiswa</h3>
            <?php if ( empty($mahasiswa) ) :?>
                <div class="alert alert-danger" role="alert">
                    Data mahasiswa tidak ditemukan.
                </div>
            <?php endif; ?>
            <ul class='list-group'>
               <?php foreach ($mahasiswa as $mhs) :?>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div class="ms-2 me-auto">
                            <div>
                                <h6><?= $mhs['nama'] ?></h6>
                            </div>
                        </div>                  
                        <a href="<?= base_url(); ?>mahasiswa/detail/<?= $mhs['id']?>" class="badge bg-primary rounded-pill ">Detail</a>
                        <a href="<?= base_url(); ?>mahasiswa/edit/<?= $mhs['id']?>" class="badge bg-warning rounded-pill mx-1">Edit</a> 
                        <a href="<?= base_url(); ?>mahasiswa/hapus/<?= $mhs['id']?>" class="badge bg-danger rounded-pill destroy">Delete</a>                    
                </li>

                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>