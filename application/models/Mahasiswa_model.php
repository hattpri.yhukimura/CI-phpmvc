<?php

class Mahasiswa_model extends CI_Model {

    public function getAllMahasiswa()
    {
       return  $this->db->get('mahasiswa')->result_array(); // select * from mahasiswa       
    } 

    public function tambahDataMahasiswa()
    {
        $data = array(
                'nama' => $this->input->post('nama', true),
                'nik' => $this->input->post('nik', true),
                'email' => $this->input->post('email', true),
                'jurusan' => $this->input->post('jurusan', true),
            );
    
        $this->db->insert('mahasiswa', $data);
    }

    public function hapusDataMahasiswa($id)
    {
    //    $this->db->where('id', $id);
       $this->db->delete('mahasiswa', ['id' => $id]);
    }

    public function getMahasiswaById($id)
    {        
       return $this->db->get_where('mahasiswa', ['id' => $id])->row_array();
    }

    public function updateDataMahasiswa($id)
    {
        $data = array(
            'nama' => $this->input->post('nama', true),
            'nik' => $this->input->post('nik', true),
            'email' => $this->input->post('email', true),
            'jurusan' => $this->input->post('jurusan', true),
        );

        $this->db->update('mahasiswa', $data, ['id' => $id]);
    }

    public function cariDataMahasiswa()
    {
       $data = $this->input->post('keyword', true);
       
       $this->db->like('nama', $data);
       $this->db->or_like('jurusan', $data);
       $this->db->or_like('nik', $data);
       $this->db->or_like('email', $data);

       return  $this->db->get('mahasiswa')->result_array();
    }
    

}