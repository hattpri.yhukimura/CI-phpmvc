<?php

class Mahasiswa extends CI_Controller {

    // __construct digunakan untuk mengkonekan database tapi kelemahanya, database hanya bisa dibuka didalam class ini aja

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mahasiswa_model'); //untuk memanggil model
        // $this->load->database(); autoload.php -> config.php
        $this->load->library('form_validation');
        
    }

    public function index()
    {        
        $data['judul'] = 'Daftar Mahasiswa';

        $data['mahasiswa'] = $this->Mahasiswa_model->getAllMahasiswa();

        if ( $this->input->post('keyword') ) {
            $data['mahasiswa'] = $this->Mahasiswa_model->cariDataMahasiswa();
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar');
        $this->load->view('mahasiswa/index', $data);
        $this->load->view('templates/footer');      
       
    }

    public function tambah()
    {
        $data['judul'] = 'Tambah Data Mahasiswa';

        $this->form_validation->set_rules('nama', 'Name', 'required');
        $this->form_validation->set_rules('nik', 'Nik', 'required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar');
            $this->load->view('mahasiswa/tambah');
            $this->load->view('templates/footer');
        } else  {
            $this->Mahasiswa_model->tambahDataMahasiswa();
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('flash', 'ditambahkan');
            redirect('mahasiswa');
        }

        
    }

    public function hapus($id)
    {
        $this->Mahasiswa_model->hapusDataMahasiswa($id);
        $this->session->set_flashdata('alert', 'danger');
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('mahasiswa');
    }

    public function detail($id)
    {
        
        $data['judul'] = 'Detail Data Mahasiswa';

        $data['mahasiswa'] = $this->Mahasiswa_model->getMahasiswaById($id);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar');
        $this->load->view('mahasiswa/detail', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id)
    {
        
        $data['judul'] = 'Detail Data Mahasiswa';

        $data['mahasiswa'] = $this->Mahasiswa_model->getMahasiswaById($id);

        $this->form_validation->set_rules('nama', 'Name', 'required');
        $this->form_validation->set_rules('nik', 'Nik', 'required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/navbar');
            $this->load->view('mahasiswa/edit', $data);
            $this->load->view('templates/footer');
        } else  {
            $this->Mahasiswa_model->updateDataMahasiswa($id);
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('flash', 'diganti');
            redirect('mahasiswa');
        }
    }   

    
}